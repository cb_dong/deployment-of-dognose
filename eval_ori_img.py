import cv2
import os
from tqdm import tqdm
import json
import torch
import numpy as np
from scipy.spatial.distance import cdist
import pdb
from megskull.graph import Function
from meghair.utils import io
from configs.config_wb import config as cfg
from modules import detection_nose, landmark_nose
from utils import utils
from albumentations.pytorch.functional import img_to_tensor
import pickle
from models import ResNet


def load_model(model_file):
    network = io.load_network(model_file)
    func = Function().compile(network.outputs)
    return func
    
if __name__ == "__main__":
    """
    提供输入：
    1.待查询图像路径/值:img_names，要求能被cv2读取
    2.数据底库：database，pickle读取; {label1:feat1, label2:feat2, ....}格式

    输出：
    query_feat为输入的特征向量，与底库中距离最小的为查询值。其余功能可在后端继续开发
    """

    img_names = ['xxx.jpg','xxx.jpg','....']
    device = torch.device('cuda')
    detection_func = load_model(cfg.detection_model)
    landmark_func = load_model(cfg.landmark_model)

    recognition_func = ResNet(2)#cfg.outdim)
    
    state_dict = torch.load(cfg.recogntion_model, map_location='cpu')
    recognition_func.load_state_dict({k: w for k, w in state_dict.items() if ('fc.0' not in k)},strict=False)
    recognition_func = recognition_func.eval().to(device)
    
    with open('database.pkl','rb') as f:
        database = pickle.load(f)
        base_feats = np.array(database.values())
        labels = list(database.keys())
    
    fails = []
    with torch.no_grad():
        for img_name in img_names:
            img = cv2.imread(img_name,cv2.IMREAD_COLOR)

            blur = utils.get_blur_score(img)
            if blur < cfg.blur_th:
                fails.append(img_name)
                print("too blur,failed")
                continue
            
            im = detection_nose(img.astype(np.float32), detection_func, cfg)
            if im is None:
                fails.append(img_name)
                print("found no nose,failed")
                continue
            im = im.astype(np.uint8)
            align_im = landmark_nose(im, landmark_func, cfg)
            
            align_im = cv2.cvtColor(align_im, cv2.COLOR_BGR2RGB)
            data = utils.transforms(image=align_im)
            inp = data["image"]
            inp = img_to_tensor(inp, {"mean": [0.485, 0.456, 0.406],"std": [0.229, 0.224, 0.225]})
            inp = inp.unsqueeze(0).to(device)
            _, feat = recognition_func(inp)
            query_feat = feat[0].cpu().numpy().reshape(1,-1)
            #query_feat = query_feat / np.sqrt((query_feat**2).sum(axis=1))
            dists = np.mean([cdist(query_feat, base_feats[:,i], "euclidean") \
                            for i in range(base_feats.shape[1])],axis=0).reshape(-1)
            
            ix = np.argmin(dists)
            if dists[ix] < cfg.distance_th:
                label = labels[ix]
                print("get %s as %s"%(img_name,label))
            else:
                print("found no such dog")
