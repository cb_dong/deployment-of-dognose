# -*- coding: utf-8 -*-

import cv2
import numpy as np
import time
import nori2

import utils
from common import config


def quality_nose(img, func):
    im = img[:,:,::-1]
    im = cv2.resize(im, config.qua_size)
    im = utils.img_to_tenosr(im)

    out = func(img=im)[0]
    pred = out[0][0]
    return pred
