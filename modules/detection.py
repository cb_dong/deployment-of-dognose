import numpy as np

from utils import utils


def detection_nose(img, func, cfg, clean=False):

    original_height, original_width, _ = img.shape

    resized_img, scale = utils.resize_img_by_short_and_max_size(img, cfg.det_short_size, cfg.det_max_size)
    resized_img = resized_img[:, :, ::-1]
    det_img = utils.img_to_tenosr(resized_img)
    scores, pred_boxes, all_anchors = func(data=det_img)

    scores = scores[0]
    pred_boxes = utils.bbox_transform_inv(all_anchors[0], pred_boxes[0])
    pred_boxes /= scale
    pred_boxes = utils._clip_boxes(pred_boxes, [original_height, original_width])

    result_boxes = []

    inds = np.where(scores > cfg.test_cls_threshold)[0]
    cls_scores = scores[inds]
    cls_bboxes = pred_boxes[inds]

    if len(cls_scores) > 0:

        idx = np.argmax(cls_scores)
        top_score = cls_scores[idx]

        if clean:
            if top_score < 0.95:
                return None

        coord = cls_bboxes[idx]
        im = img[int(coord[1]):int(coord[3]), int(coord[0]):int(coord[2]), :]
    else:
        im = None

    return im
