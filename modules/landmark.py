import cv2
import numpy as np 

from utils import utils


def landmark_nose(im, func, cfg, rate=None, clean=False, crop=False):
    # im = cv2.resize(im, config.image_size, interpolation=cv2.BORDER_REPLICATE)
    image_size = (im.shape[1], im.shape[0])
    scale = np.array(image_size)

    ld_img = cv2.resize(im, cfg.ld_size)
    ld_img = utils.img_to_tenosr(ld_img)

    ld = func(img=ld_img)
    ld = ld[0].reshape((-1,2))
    ld = ld * scale

    aligned_im, aligned_ld = utils.align_image(ld, im, image_size) #(W, H)
    # for i in range(len(aligned_ld)):
    #     cv2.circle(aligned_im, (int(aligned_ld[i,0]),int(aligned_ld[i,1])), 5, (255,0,0), -1)

    if clean:
        angle_rate = utils.get_angle_rate(aligned_ld)

        if angle_rate > 1.4 or angle_rate < 0.8:
            return None

    if rate is None:
        image = aligned_im
    else:
        h, w, _ = aligned_im.shape
        pad_h = int(h * rate)
        pad_w = int(w * rate)
        image = utils.nose_mask(aligned_im, aligned_ld, pad_h, pad_w)

    if crop:
        image = utils.crop_patch(aligned_im, aligned_ld)
    else:
        image = aligned_im

    return image
