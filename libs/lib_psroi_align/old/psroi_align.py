#!/usr/bin/env mdl
# This file will seal the psroi_pooling opr
import numpy as np
import ctypes
import megbrain as mgb
from megbrain.craniotome import CraniotomeBase
from megbrain.craniotome import make_opr
from megskull.opr.all import MGBOprForwarderBase
from megskull.graph import as_varnode
import os, sys, struct
from IPython import embed
from time import time

_TYPE_POINTER = ctypes.c_void_p
_TYPE_INT = ctypes.c_int32
_TYPE_FLOAT = ctypes.c_float

_so_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lib_psroi_align.so')
_so_lib  = ctypes.CDLL(_so_path)

_so_lib.PSROIAlignForwardGpu.argtypes = [_TYPE_POINTER, _TYPE_POINTER, _TYPE_POINTER, _TYPE_FLOAT, _TYPE_INT, _TYPE_INT, _TYPE_INT, _TYPE_INT, _TYPE_INT]
_so_lib.PSROIAlignForwardGpu.restype = None

_so_lib.PSROIAlignBackwardGpu.argtypes = [_TYPE_POINTER, _TYPE_POINTER, _TYPE_POINTER, _TYPE_FLOAT, _TYPE_INT, _TYPE_INT, _TYPE_INT, _TYPE_INT, _TYPE_INT]
_so_lib.PSROIAlignBackwardGpu.restype = None


class PSROIAlignBackwardImpl(CraniotomeBase):
    __nr_inputs__ = 3
    __nr_outputs__= 1
    def setup(self, spatial_scale, pooled_height, pooled_width, sample_height, sample_width):
        self._spatial_scale = spatial_scale
        self._pooled_height = pooled_height
        self._pooled_width = pooled_width
        self._phw = self._pooled_width * self._pooled_height

        self._sample_height = sample_height
        self._sample_width  = sample_width

    def execute(self, inputs, outputs):
        rois, feat_map, pspooled_grad = inputs
        feat_map_grad = outputs[0]

        rois_tensor_ptr = rois.pubapi_dev_tensor_ptr
        pspooled_grad_tensor_ptr = pspooled_grad.pubapi_dev_tensor_ptr
        feat_map_grad_tensor_ptr = feat_map_grad.pubapi_dev_tensor_ptr

        _so_lib.PSROIAlignBackwardGpu(rois_tensor_ptr, pspooled_grad_tensor_ptr, feat_map_grad_tensor_ptr,
                self._spatial_scale, self._pooled_channel, self._pooled_height, self._pooled_width,
                self._sample_height, self._sample_width)


    def infer_shape(self, inp_shapes):
        # infer the output shapes of the PSROIAlign
        rois_shape = inp_shapes[0]
        fm_shape = inp_shapes[1]

        self._pooled_channel = fm_shape[1] / (self._phw)
        assert self._pooled_channel == int(self._pooled_channel)
        self._pooled_channel = int(self._pooled_channel)
        return (fm_shape, )

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        return 0

class PSROIAlignImpl(CraniotomeBase):
    __nr_inputs__ = 2
    __nr_outputs__= 1

    def setup(self, spatial_scale, pooled_height, pooled_width, sample_height, sample_width):
        self._spatial_scale = spatial_scale
        self._pooled_height = pooled_height
        self._pooled_width = pooled_width
        self._phw = self._pooled_width * self._pooled_height

        self._sample_height = sample_height
        self._sample_width  = sample_width

    def execute(self, inputs, outputs):
        # invoke the
        rois, feat_map = inputs
        pspooled_map = outputs[0]

        rois_tensor_ptr = rois.pubapi_dev_tensor_ptr
        fm_tensor_ptr = feat_map.pubapi_dev_tensor_ptr
        pspooled_tensor_ptr = pspooled_map.pubapi_dev_tensor_ptr

        _so_lib.PSROIAlignForwardGpu(rois_tensor_ptr, fm_tensor_ptr, pspooled_tensor_ptr,
                self._spatial_scale, self._pooled_channel, self._pooled_height, self._pooled_width,
                self._sample_height, self._sample_width)

    def get_serialize_params(self):
        return ('psroi_pooling', struct.pack('fii', self._spatial_scale, self._pooled_height, self._pooled_width))

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        if wrt_idx == 1:
            return PSROIAlignBackwardImpl.make(inputs[0], inputs[1], out_grad[0], spatial_scale=self._spatial_scale, pooled_height=self._pooled_height,\
                                                pooled_width=self._pooled_width, sample_height=self._sample_height, sample_width=self._sample_width)
        else:
            return 0

    def infer_shape(self, inp_shapes):
        # infer the output shapes of the PSROIAlign
        rois_shape = inp_shapes[0]
        fm_shape = inp_shapes[1]

        self._pooled_channel = fm_shape[1] / (self._phw)
        assert self._pooled_channel == int(self._pooled_channel)
        self._pooled_channel = int(self._pooled_channel)
        return [[rois_shape[0], self._pooled_channel, self._pooled_height, self._pooled_width] ]

# decorates the RPNTargetCran as the Megskull Operator
class PSROIAlign(MGBOprForwarderBase):
    _spatial_scale = None
    _pooled_height = None
    _pooled_width  = None
    _sample_height = None
    _sample_width  = None

    def __init__(self, name, rois, feat_map, spatial_scale, pooled_height, pooled_width, sample_height, sample_width):
        rois = as_varnode(rois)
        feat_map = as_varnode(feat_map)
        super().__init__(name=name, inputs=[rois, feat_map])
        self._spatial_scale = spatial_scale
        self._pooled_height= pooled_height
        self._pooled_width = pooled_width

        self._sample_height = sample_height
        self._sample_width = sample_width

    def _mgb_func(self, env, *inputs):
        rois, feat_map = inputs
        return PSROIAlignImpl.make(rois, feat_map, spatial_scale=self._spatial_scale, pooled_height=self._pooled_height,\
                                    pooled_width=self._pooled_width, sample_height=self._sample_height, sample_width=self._sample_width)
