#!/usr/bin/env mdl
# ROIAlign module
# Copyright(c) 2017 Megvii Technology Limited
# Written by Mengxiao Lin

import numpy as np
import ctypes
import megbrain as mgb
from megbrain.craniotome import CraniotomeBase
from megbrain.craniotome import make_opr
from megskull.opr.all import MGBOprForwarderBase
from megskull.graph import as_varnode
from megskull.utils.misc import get_2dshape
import os

_roi_align_so_path = os.path.dirname(os.path.abspath(__file__))
_roi_align_so = ctypes.CDLL(os.path.join(_roi_align_so_path,'lib_roialign.so'))
#TYPE_FLOAT_S = ctypes.POINTER(ctypes.c_float)
TYPE_FLOAT_S = ctypes.c_void_p
TYPE_INT_S = ctypes.c_void_p
TYPE_INT = ctypes.c_int32
TYPE_FLOAT = ctypes.c_float
TYPE_POINTER = ctypes.c_void_p

_roi_align_so.ROIAlignForwardGpuV2.argtypes = [
    TYPE_POINTER, TYPE_POINTER, TYPE_FLOAT, TYPE_FLOAT, TYPE_INT, TYPE_INT, TYPE_INT,
    TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_POINTER
]
_roi_align_so.ROIAlignForwardGpuV2.restype = None

_roi_align_so.ROIAlignBackwardGpuV2.argtypes = [
        TYPE_POINTER, TYPE_POINTER, TYPE_FLOAT, TYPE_FLOAT, TYPE_INT, TYPE_INT, TYPE_INT,
        TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_POINTER, TYPE_INT, TYPE_POINTER
]
_roi_align_so.ROIAlignBackwardGpuV2.restype = None

_roi_align_so.ROIAlignMaxForwardGpuV2.argtypes = [
    TYPE_POINTER, TYPE_POINTER, TYPE_FLOAT, TYPE_FLOAT, TYPE_INT, TYPE_INT, TYPE_INT,
    TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_POINTER, TYPE_POINTER
]
_roi_align_so.ROIAlignMaxForwardGpuV2.restype = None

_roi_align_so.ROIAlignMaxForwardGpu.argtypes = [
    TYPE_POINTER, TYPE_POINTER, TYPE_FLOAT, TYPE_FLOAT, TYPE_INT, TYPE_INT, TYPE_INT,
    TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_POINTER, TYPE_POINTER
]
_roi_align_so.ROIAlignMaxForwardGpu.restype = None


_roi_align_so.ROIAlignMaxBackwardGpuV2.argtypes = [
        TYPE_POINTER, TYPE_POINTER, TYPE_FLOAT, TYPE_FLOAT, TYPE_INT, TYPE_INT, TYPE_INT,
        TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_FLOAT_S, TYPE_INT, TYPE_POINTER, TYPE_POINTER
]
_roi_align_so.ROIAlignMaxBackwardGpuV2.restype = None

class ROIAlign2DFwdImpl(CraniotomeBase):
    __nr_inputs__ = 2
    __nr_outputs__ = 1
    def setup(self, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2):
        self.spatial_scale = TYPE_FLOAT(spatial_scale)
        self.offset = TYPE_FLOAT(offset)
        self.pooled_height = TYPE_INT(pooled_height)
        self.pooled_width = TYPE_INT(pooled_width)
        self.sample_height = TYPE_INT(sample_height)
        self.sample_width = TYPE_INT(sample_width)

    def execute(self, inputs, outputs):
        # data: B*C*H*W
        # rois: M*(1+4)
        data, rois = inputs
        pool_output = outputs[0]
        channels = TYPE_INT(data.shape[1])
        data_height = TYPE_INT(data.shape[2])
        data_width = TYPE_INT(data.shape[3])

        data_dptr = TYPE_POINTER(data.pubapi_dev_tensor_ptr)
        rois_dptr = TYPE_POINTER(rois.pubapi_dev_tensor_ptr)
        output_dptr = TYPE_POINTER(pool_output.pubapi_dev_tensor_ptr)
        output_count = TYPE_INT(rois.shape[0] * data.shape[1] * self.pooled_height.value * self.pooled_width.value)

        _roi_align_so.ROIAlignForwardGpuV2(data_dptr, rois_dptr,
                self.spatial_scale, self.offset, channels, data_height, data_width,
                self.pooled_height, self.pooled_width, self.sample_height, self.sample_width,
                output_count, output_dptr)

    def infer_shape(self, inp_shapes):
        batch_size, channels, _, __ = inp_shapes[0]
        rois_count, rois_each_item = inp_shapes[1]
        assert(rois_each_item == 5)
        return ((rois_count, channels, self.pooled_height.value, self.pooled_width.value), )

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        if wrt_idx != 0:
            return 0
        return ROIAlign2DBwdImpl.make(inputs[0], inputs[1], outputs[0], out_grad[0], name=inputs[0].name+'_roi_align_grad',
                spatial_scale=self.spatial_scale.value, offset=self.offset.value, pooled_height=self.pooled_height.value, pooled_width=self.pooled_width.value,
                sample_height=self.sample_height.value, sample_width=self.sample_width.value)

class ROIAlign2DBwdImpl(CraniotomeBase):
    __nr_inputs__ = 4
    __nr_outputs__ = 1
    def setup(self, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2):
        self.spatial_scale = TYPE_FLOAT(spatial_scale)
        self.offset = TYPE_FLOAT(offset)
        self.pooled_height = TYPE_INT(pooled_height)
        self.pooled_width = TYPE_INT(pooled_width)
        self.sample_height = TYPE_INT(sample_height)
        self.sample_width = TYPE_INT(sample_width)

    def execute(self, inputs, outputs):
        data = inputs[0]
        rois = inputs[1]
        pooled_out = inputs[2]
        pooled_grad = inputs[3]

        channels = TYPE_INT(data.shape[1])
        data_height = TYPE_INT(data.shape[2])
        data_width = TYPE_INT(data.shape[3])

        data_dptr = TYPE_POINTER(data.pubapi_dev_tensor_ptr)
        rois_dptr = TYPE_POINTER(rois.pubapi_dev_tensor_ptr)
        output_dptr = TYPE_POINTER(pooled_out.pubapi_dev_tensor_ptr)
        output_grad_dptr = TYPE_POINTER(pooled_grad.pubapi_dev_tensor_ptr)

        data_grad = outputs[0]
        data_grad_dptr = TYPE_POINTER(data_grad.pubapi_dev_tensor_ptr)
        data_count = TYPE_INT(data.shape[0] * channels.value * data_height.value * data_width.value)
        output_count = TYPE_INT(pooled_out.shape[0] * channels.value * self.pooled_height.value * self.pooled_width.value)

        _roi_align_so.ROIAlignBackwardGpuV2(data_dptr, rois_dptr,
                self.spatial_scale, self.offset, channels, data_height, data_width,
                self.pooled_height, self.pooled_width, self.sample_height, self.sample_width,
                output_count, output_grad_dptr, data_count, data_grad_dptr)

    def infer_shape(self, inp_shapes):
        return (inp_shapes[0], )

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        return 0


class ROIAlignMax2DFwdImpl(CraniotomeBase):
    __nr_inputs__ = 2
    __nr_outputs__ = 2
    def setup(self, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2):
        self.spatial_scale = TYPE_FLOAT(spatial_scale)
        self.offset = TYPE_FLOAT(offset)
        self.pooled_height = TYPE_INT(pooled_height)
        self.pooled_width = TYPE_INT(pooled_width)
        self.sample_height = TYPE_INT(sample_height)
        self.sample_width = TYPE_INT(sample_width)

    def execute(self, inputs, outputs):
        # data: B*C*H*W
        # rois: M*(1+4)
        data, rois = inputs
        pool_output, max_buffer = outputs
        channels = TYPE_INT(data.shape[1])
        data_height = TYPE_INT(data.shape[2])
        data_width = TYPE_INT(data.shape[3])

        data_dptr = TYPE_POINTER(data.pubapi_dev_tensor_ptr)
        rois_dptr = TYPE_POINTER(rois.pubapi_dev_tensor_ptr)
        output_dptr = TYPE_POINTER(pool_output.pubapi_dev_tensor_ptr)
        max_buffer_dptr = TYPE_POINTER(max_buffer.pubapi_dev_tensor_ptr)

        output_count = TYPE_INT(rois.shape[0] * data.shape[1] * self.pooled_height.value * self.pooled_width.value)

        _roi_align_so.ROIAlignMaxForwardGpuV2(data_dptr, rois_dptr,
                self.spatial_scale, self.offset, channels, data_height, data_width,
                self.pooled_height, self.pooled_width, self.sample_height, self.sample_width,
                output_count, output_dptr, max_buffer_dptr)

    def infer_shape(self, inp_shapes):
        batch_size, channels, _, __ = inp_shapes[0]
        rois_count, rois_each_item = inp_shapes[1]
        assert(rois_each_item == 5)
        return ((rois_count, channels, self.pooled_height.value, self.pooled_width.value),
                (rois_count, channels, self.pooled_height.value, self.pooled_width.value), )

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        if wrt_idx != 0:
            return 0
        return ROIAlignMax2DBwdImpl.make(inputs[0], inputs[1], outputs[0], outputs[1], out_grad[0], name=inputs[0].name+'_roi_align_grad',
                spatial_scale=self.spatial_scale.value, offset=self.offset.value, pooled_height=self.pooled_height.value, pooled_width=self.pooled_width.value,
                sample_height=self.sample_height.value, sample_width=self.sample_width.value)

class ROIAlignMax2DBwdImpl(CraniotomeBase):
    __nr_inputs__ = 5
    __nr_outputs__ = 1
    def setup(self, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2):
        self.spatial_scale = TYPE_FLOAT(spatial_scale)
        self.offset = TYPE_FLOAT(offset)
        self.pooled_height = TYPE_INT(pooled_height)
        self.pooled_width = TYPE_INT(pooled_width)
        self.sample_height = TYPE_INT(sample_height)
        self.sample_width = TYPE_INT(sample_width)

    def execute(self, inputs, outputs):
        data = inputs[0]
        rois = inputs[1]
        pooled_out = inputs[2]
        max_buffer = inputs[3]
        pooled_grad = inputs[4]

        channels = TYPE_INT(data.shape[1])
        data_height = TYPE_INT(data.shape[2])
        data_width = TYPE_INT(data.shape[3])

        data_dptr = TYPE_POINTER(data.pubapi_dev_tensor_ptr)
        rois_dptr = TYPE_POINTER(rois.pubapi_dev_tensor_ptr)
        output_dptr = TYPE_POINTER(pooled_out.pubapi_dev_tensor_ptr)
        max_buffer_dptr= TYPE_POINTER(max_buffer.pubapi_dev_tensor_ptr)
        output_grad_dptr = TYPE_POINTER(pooled_grad.pubapi_dev_tensor_ptr)

        data_grad = outputs[0]
        data_grad_dptr = TYPE_POINTER(data_grad.pubapi_dev_tensor_ptr)
        data_count = TYPE_INT(data.shape[0] * channels.value * data_height.value * data_width.value)
        output_count = TYPE_INT(pooled_out.shape[0] * channels.value * self.pooled_height.value * self.pooled_width.value)

        _roi_align_so.ROIAlignMaxBackwardGpuV2(data_dptr, rois_dptr,
                self.spatial_scale, self.offset, channels, data_height, data_width,
                self.pooled_height, self.pooled_width, self.sample_height, self.sample_width,
                output_count, output_grad_dptr, data_count, max_buffer_dptr, data_grad_dptr)

    def infer_shape(self, inp_shapes):
        return (inp_shapes[0], )

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        return 0

# class roi_align_2d(MGBOprForwarderBase):
class ROIAlign(MGBOprForwarderBase):
    # def __init__(self, src, rois, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2, mode='AVERAGE', name=None):
    spatial_scale = None
    offset = None
    pooled_height = None
    pooled_width  = None
    sample_height = None
    sample_width  = None
    mode = None

    def __init__(self, name, src, rois, pooled_shape, sample_shape, scale, mode, *, offset=0):
        src = as_varnode(src)
        rois = as_varnode(rois)
        if name is None:
            name = "roi_align_of:{}".format(src.name)
        super().__init__(inputs=[src, rois], name=name)
        pooled_shape = get_2dshape(pooled_shape)
        sample_shape = get_2dshape(sample_shape)

        self.spatial_scale = scale
        self.offset = offset
        self.pooled_height = pooled_shape[0]
        self.pooled_width = pooled_shape[1]
        self.sample_height = sample_shape[0]
        self.sample_width = sample_shape[1]
        self.mode = mode.upper()
        if mode != 'AVERAGE' and mode != 'MAX':
            raise NotImplementedError

    def _mgb_func(self, env, *inputs):
        data, rois = inputs
        if self.mode =='AVERAGE':
            return ROIAlign2DFwdImpl.make(data, rois, name=self.name+'_forward',
                    spatial_scale=self.spatial_scale, offset=self.offset, pooled_height=self.pooled_height, pooled_width=self.pooled_width,
                    sample_height=self.sample_height, sample_width=self.sample_width)
        else:
            return ROIAlignMax2DFwdImpl.make(data, rois, name=self.name+'_forward',
                    spatial_scale=self.spatial_scale, offset=self.offset, pooled_height=self.pooled_height, pooled_width=self.pooled_width,
                    sample_height=self.sample_height, sample_width=self.sample_width)[0]

