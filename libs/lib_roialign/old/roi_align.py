#!/usr/bin/env mdl
# ROIAlign module
# Copyright(c) 2017 Megvii Technology Limited 
# Written by Mengxiao Lin

import numpy as np
import ctypes
import megbrain as mgb
from megbrain.craniotome import CraniotomeBase
from megbrain.craniotome import make_opr
from megskull.opr.all import MGBOprForwarderBase
from megskull.graph import as_varnode
import os 

_roi_align_so_path = os.path.dirname(os.path.abspath(__file__))
_roi_align_so = ctypes.CDLL(os.path.join(_roi_align_so_path,'lib_roialign.so'))
#TYPE_FLOAT_S = ctypes.POINTER(ctypes.c_float)
TYPE_FLOAT_S = ctypes.c_void_p
TYPE_INT = ctypes.c_int32
TYPE_FLOAT = ctypes.c_float
# void ROIAlignForwardGpu(const float* bottom_data, const float* bottom_rois, 
#         const float spatial_scale_, const float offset_, const int channels_, const int height_, const int width_, 
#         const int pooled_height_, const int pooled_width_, const int sample_height_, const int sample_width_,
#         const int top_count, float* top_data);
_roi_align_so.ROIAlignForwardGpu.argtypes = [
    TYPE_FLOAT_S, TYPE_FLOAT_S, TYPE_FLOAT, TYPE_FLOAT, TYPE_INT, TYPE_INT, TYPE_INT,
    TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_FLOAT_S
]
_roi_align_so.ROIAlignForwardGpu.restype = None

# void ROIAlignBackwardGpu(const float* bottom_data, const float* bottom_rois, 
#         const float spatial_scale_, const float offset_, const int channels_, const int height_, const int width_, 
#         const int pooled_height_, const int pooled_width_, const int sample_height_, const int sample_width_,
#         const int top_count, const float* top_diff, const int bottom_count, float* bottom_diff);
_roi_align_so.ROIAlignBackwardGpu.argtypes = [
        TYPE_FLOAT_S, TYPE_FLOAT_S, TYPE_FLOAT, TYPE_FLOAT, TYPE_INT, TYPE_INT, TYPE_INT, 
        TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_FLOAT_S, TYPE_INT, TYPE_FLOAT_S
]
_roi_align_so.ROIAlignBackwardGpu.restype = None

class ROIAlign2DFwdImpl(CraniotomeBase):
    __nr_inputs__ = 2
    __nr_outputs__ = 1
    def setup(self, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2):
        self.spatial_scale = TYPE_FLOAT(spatial_scale)
        self.offset = TYPE_FLOAT(offset)
        self.pooled_height = TYPE_INT(pooled_height)
        self.pooled_width = TYPE_INT(pooled_width)
        self.sample_height = TYPE_INT(sample_height)
        self.sample_width = TYPE_INT(sample_width)

    def execute(self, inputs, outputs):
        # data: B*C*H*W
        # rois: M*(1+4)
        data, rois = inputs
        pool_output = outputs[0]
        channels = TYPE_INT(data.shape[1])
        data_height = TYPE_INT(data.shape[2])
        data_width = TYPE_INT(data.shape[3])
        
        data_dptr = TYPE_FLOAT_S(data.dev_ptr)
        rois_dptr = TYPE_FLOAT_S(rois.dev_ptr)
        output_dptr = TYPE_FLOAT_S(pool_output.dev_ptr)
        output_count = TYPE_INT(rois.shape[0] * data.shape[1] * self.pooled_height.value * self.pooled_width.value)

        _roi_align_so.ROIAlignForwardGpu(data_dptr, rois_dptr, 
                self.spatial_scale, self.offset, channels, data_height, data_width, 
                self.pooled_height, self.pooled_width, self.sample_height, self.sample_width,
                output_count, output_dptr)

    def infer_shape(self, inp_shapes):
        batch_size, channels, _, __ = inp_shapes[0]
        rois_count, rois_each_item = inp_shapes[1]
        assert(rois_each_item == 5)
        return ((rois_count, channels, self.pooled_height.value, self.pooled_width.value), )
    
    def grad(self, wrt_idx, inputs, outputs, out_grad):
        if wrt_idx != 0:
            return 0
        return ROIAlign2DBwdImpl.make(inputs[0], inputs[1], outputs[0], out_grad[0], name=inputs[0].name+'_roi_align_grad', 
                spatial_scale=self.spatial_scale.value, offset=self.offset.value, pooled_height=self.pooled_height.value, pooled_width=self.pooled_width.value,
                sample_height=self.sample_height.value, sample_width=self.sample_width.value)
        
class ROIAlign2DBwdImpl(CraniotomeBase):
    __nr_inputs__ = 4
    __nr_outputs__ = 1
    def setup(self, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2):
        self.spatial_scale = TYPE_FLOAT(spatial_scale)
        self.offset = TYPE_FLOAT(offset)
        self.pooled_height = TYPE_INT(pooled_height)
        self.pooled_width = TYPE_INT(pooled_width)
        self.sample_height = TYPE_INT(sample_height)
        self.sample_width = TYPE_INT(sample_width)
    
    def execute(self, inputs, outputs):
        data = inputs[0]
        rois = inputs[1]
        pooled_out = inputs[2]
        pooled_grad = inputs[3]
        
        channels = TYPE_INT(data.shape[1])
        data_height = TYPE_INT(data.shape[2])
        data_width = TYPE_INT(data.shape[3])

        data_dptr = TYPE_FLOAT_S(data.dev_ptr)
        rois_dptr = TYPE_FLOAT_S(rois.dev_ptr)
        output_dptr = TYPE_FLOAT_S(pooled_out.dev_ptr)
        output_grad_dptr = TYPE_FLOAT_S(pooled_grad.dev_ptr)

        data_grad = outputs[0]
        data_grad_dptr = TYPE_FLOAT_S(data_grad.dev_ptr)
        data_count = TYPE_INT(data.shape[0] * channels.value * data_height.value * data_width.value)
        output_count = TYPE_INT(pooled_out.shape[0] * channels.value * self.pooled_height.value * self.pooled_width.value)

        _roi_align_so.ROIAlignBackwardGpu(data_dptr, rois_dptr, 
                self.spatial_scale, self.offset, channels, data_height, data_width,
                self.pooled_height, self.pooled_width, self.sample_height, self.sample_width,
                output_count, output_grad_dptr, data_count, data_grad_dptr)
        
    def infer_shape(self, inp_shapes):
        return (inp_shapes[0], )

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        return 0

class roi_align_2d(MGBOprForwarderBase):
    def __init__(self, src, rois, spatial_scale, offset, pooled_height, pooled_width, sample_height=2, sample_width=2, name=None):
        src = as_varnode(src)
        rois = as_varnode(rois)
        if name is None:
            name = "roi_align_of:{}".format(src.name)
        super().__init__(inputs=[src, rois], name=name)
        self.spatial_scale = spatial_scale
        self.offset = offset
        self.pooled_height = pooled_height
        self.pooled_width = pooled_width
        self.sample_height = sample_height
        self.sample_width = sample_width

    def _mgb_func(self, env, *inputs):
        data, rois = inputs
        return ROIAlign2DFwdImpl.make(data, rois, name=self.name+'_forward', 
                spatial_scale=self.spatial_scale, offset=self.offset, pooled_height=self.pooled_height, pooled_width=self.pooled_width, 
                sample_height=self.sample_height, sample_width=self.sample_width)

