// ------------------------------------------------------------------
// ROI Align Module
// Copyright (c) 2017 Megvii Technology Limited
// Written by Mengxiao Lin
// ------------------------------------------------------------------

#define CUDA_KERNEL_LOOP(i, n) \
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; \
       i < (n); \
       i += blockDim.x * gridDim.x)

#define CAFFE_CUDA_NUM_THREADS 512
static int _ROI_ALIGN_GET_BLOCKS(const int N) {
  return (N + CAFFE_CUDA_NUM_THREADS - 1) / CAFFE_CUDA_NUM_THREADS;
}

__device__ static float ROIAlignGetCoeff(float dh, float dw){
     dw = dw > 0 ? dw : -dw;
     dh = dh > 0 ? dh : -dh;
     return (1.0f - dh) * (1.0f - dw);
}

/**
  * Implementation of the bilinear interpolation.
  */
__device__ static float ROIAlignGetInterpolating(const float* data, const float h, const float w, const int height, const int width){
    float retVal = 0.0f;
    int h1 = floorf(h);
    int w1 = floorf(w);
    bool overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    retVal += overflow? 0.0f : data[h1 * width + w1] * ROIAlignGetCoeff(h - float(h1), w - float(w1));
    h1 = ceilf(h);
    w1 = floorf(w);
    overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    retVal += overflow? 0.0f : data[h1 * width + w1] * ROIAlignGetCoeff(h - float(h1), w - float(w1));
    h1 = floorf(h);
    w1 = ceilf(w);
    overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    retVal += overflow? 0.0f : data[h1 * width + w1] * ROIAlignGetCoeff(h - float(h1), w - float(w1));
    h1 = ceilf(h);
    w1 = ceilf(w);
    overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    retVal += overflow? 0.0f : data[h1 * width + w1] * ROIAlignGetCoeff(h - float(h1), w - float(w1));
    return retVal;
}
/**
  * Get the derivative of the bilinear interpolation.
  */
__device__ static void ROIAlignDistributeDiff(float* diff, const float top_diff, const float h, const float w, const int height, const int width){
    int h1 = floorf(h);
    int w1 = floorf(w);
    bool overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    if (!overflow){
      atomicAdd(diff + h1 * width + w1, top_diff * ROIAlignGetCoeff(h - float(h1), w - float(w1)));
    }
    h1 = ceilf(h);
    w1 = floorf(w);
    overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    if (!overflow){
      atomicAdd(diff + h1 * width + w1, top_diff * ROIAlignGetCoeff(h - float(h1), w - float(w1)));
    }
    h1 = floorf(h);
    w1 = ceilf(w);
    overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    if (!overflow){
      atomicAdd(diff + h1 * width + w1, top_diff * ROIAlignGetCoeff(h - float(h1), w - float(w1)));
    }
    h1 = ceilf(h);
    w1 = ceilf(w);
    overflow = (h1<0) || (w1<0) || (h1 >=height) || (w1>=width);
    if (!overflow){
      atomicAdd(diff + h1 * width + w1, top_diff * ROIAlignGetCoeff(h - float(h1), w - float(w1)));
    }
}

__global__ void ROIAlignForward(const int nthreads, const float* bottom_data,
    const float spatial_scale, const float offset, const int channels, const int height,
    const int width, const int pooled_height, const int pooled_width,
    const int sample_height, const int sample_width,
    const float* bottom_rois, float* top_data) {
  CUDA_KERNEL_LOOP(index, nthreads) {
    // (n, c, ph, pw) is an element in the pooled output
    int pw = index % pooled_width;
    int ph = (index / pooled_width) % pooled_height;
    int c = (index / pooled_width / pooled_height) % channels;
    int n = index / pooled_width / pooled_height / channels;

    bottom_rois += n * 5;
    int roi_batch_ind = bottom_rois[0];
    float roi_start_w = (bottom_rois[1] - offset) * spatial_scale;
    float roi_start_h = (bottom_rois[2] - offset) * spatial_scale;
    float roi_end_w = (bottom_rois[3] - offset) * spatial_scale;
    float roi_end_h = (bottom_rois[4] - offset) * spatial_scale;

    float roi_width = max(roi_end_w - roi_start_w, ((float)0.0));
    float roi_height = max(roi_end_h - roi_start_h, ((float)0.0));
    float bin_size_h = static_cast<float>(roi_height)
                       / static_cast<float>(pooled_height);
    float bin_size_w = static_cast<float>(roi_width)
                       / static_cast<float>(pooled_width);

    // regularly sample from a sample_height*sample_width grid
    bottom_data += (roi_batch_ind * channels + c) * height * width;
    float sample_h_rate = 1.0f / float(sample_height);
    float sample_w_rate = 1.0f / float(sample_width);
    float hcenter;
    float wcenter;
    
    float tmp = float(0.);
    for (int h_iter = 0; h_iter < sample_height; ++h_iter){
        for (int w_iter = 0; w_iter < sample_width; ++w_iter){
            hcenter = roi_start_h + bin_size_h * (ph + sample_h_rate * (h_iter + 0.5f));
            wcenter = roi_start_w + bin_size_w * (pw + sample_w_rate * (w_iter + 0.5f));
            tmp += ROIAlignGetInterpolating(bottom_data, hcenter, wcenter, height, width);
        }
    } 
    top_data[index] = tmp / sample_height / sample_width;
  }
}

__global__ void ROIAlignBackward(const int nthreads, const float* top_diff,
    const float spatial_scale, const float offset,
    const int channels, const int height, const int width,
    const int pooled_height, const int pooled_width, 
    const int sample_height, const int sample_width,
    float* bottom_diff, const float* bottom_rois) {

  CUDA_KERNEL_LOOP(index, nthreads) {
    // (n, c, ph, pw) is an element in the pooled output
    int pw = index % pooled_width;
    int ph = (index / pooled_width) % pooled_height;
    int c = (index / pooled_width / pooled_height) % channels;
    int n = index / pooled_width / pooled_height / channels;

    bottom_rois += n * 5;
    int roi_batch_ind = bottom_rois[0];
    float roi_start_w = (bottom_rois[1] - offset) * spatial_scale;
    float roi_start_h = (bottom_rois[2] - offset) * spatial_scale;
    float roi_end_w = (bottom_rois[3] - offset) * spatial_scale;
    float roi_end_h = (bottom_rois[4] - offset) * spatial_scale;

    float roi_width = max(roi_end_w - roi_start_w, (float)0);
    float roi_height = max(roi_end_h - roi_start_h, (float)0);
    float bin_size_h = static_cast<float>(roi_height)
                       / static_cast<float>(pooled_height);
    float bin_size_w = static_cast<float>(roi_width)
                       / static_cast<float>(pooled_width);

    bottom_diff += (roi_batch_ind * channels + c) * height * width;
    
    float sample_h_rate = 1.0f / float(sample_height);
    float sample_w_rate = 1.0f / float(sample_width);
    float hcenter;
    float wcenter;
    
    float tmp = top_diff[index] / sample_height / sample_width;
    for (int h_iter = 0; h_iter < sample_height; ++h_iter){
      for (int w_iter = 0; w_iter < sample_width; ++w_iter){
        hcenter = roi_start_h + bin_size_h * (ph + sample_h_rate * (h_iter + 0.5f));
        wcenter = roi_start_w + bin_size_w * (pw + sample_w_rate * (w_iter + 0.5f));
        ROIAlignDistributeDiff(bottom_diff, tmp, hcenter, wcenter, height, width);
      }
    } 
  }
}

// interface outside
extern "C"{

void ROIAlignForwardGpu(const void* bottom_data_p, const void* bottom_rois_p, 
        const float spatial_scale_, const float offset_, const int channels_, const int height_, const int width_, 
        const int pooled_height_, const int pooled_width_, const int sample_height_, const int sample_width_,
        const int top_count, void* top_data_p) {
  //const float* bottom_data = bottom[0]->gpu_data();
  //const float* bottom_rois = bottom[1]->gpu_data();
  //float* top_data = top[0]->mutable_gpu_data();
  //int count = top[0]->count();
  const float* bottom_data = (const float*) bottom_data_p;
  const float* bottom_rois = (const float*) bottom_rois_p;
  float* top_data = (float *) top_data_p;
  cudaDeviceSynchronize();
  ROIAlignForward<<<_ROI_ALIGN_GET_BLOCKS(top_count), CAFFE_CUDA_NUM_THREADS>>>(
      top_count, bottom_data, spatial_scale_, offset_, channels_, height_, width_,
      pooled_height_, pooled_width_, sample_height_, sample_width_, bottom_rois, top_data);
  cudaDeviceSynchronize();
  //CUDA_POST_KERNEL_CHECK;
}

void ROIAlignBackwardGpu(const void* bottom_data_p, const void* bottom_rois_p, 
        const float spatial_scale_, const float offset_, const int channels_, const int height_, const int width_, 
        const int pooled_height_, const int pooled_width_, const int sample_height_, const int sample_width_,
        const int top_count, const void* top_diff_p, const int bottom_count, void* bottom_diff_p) {
  const float* bottom_data = (const float*) bottom_data_p;
  const float* bottom_rois = (const float*) bottom_rois_p;
  const float* top_diff = (const float *) top_diff_p;
  float* bottom_diff = (float*) bottom_diff_p;
  //const float* bottom_rois = bottom[1]->gpu_data();
  //const float* top_diff = top[0]->gpu_diff();
  //float* bottom_diff = bottom[0]->mutable_gpu_diff();
  //const int count = top[0]->count();
  //const int bottom_diff_count = bottom[0]->count();
  cudaDeviceSynchronize();
  cudaMemset(bottom_diff, 0, sizeof(float) * bottom_count);
  // NOLINT_NEXT_LINE(whitespace/operators)
  ROIAlignBackward<<<_ROI_ALIGN_GET_BLOCKS(top_count), CAFFE_CUDA_NUM_THREADS>>>(
      top_count, top_diff, spatial_scale_, offset_, channels_,
      height_, width_, pooled_height_, pooled_width_, sample_height_, sample_width_, bottom_diff, bottom_rois);
  // CUDA_POST_KERNEL_CHECK;
  cudaDeviceSynchronize();
}

}
