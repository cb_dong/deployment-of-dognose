# ROI Align Module for MegBrain

### Usage

The python file `roi_align.py` export a class named `roi_align_2d`, which is a MegSkull operator.

```
# suppose symbol of input features is `f` and rois is `rois`
# original roi pool is 
pool = ROIPooling2D('pool5', f, rois, (7, 7), scale=1.0/16)

# roi align should be used as following code:
pool = roi_align_2d(f, rois, spatial_scale=1.0/16, offset=0.0, pooled_height=7, pooled_width=7, sample_height=2, sample_width=2, name='pool5')
```

More details could be found in Mask R-CNN (arXiv 1703.06870).

### Bug Report

Contact Mengxiao Lin(linmengxiao@megvii.com).
