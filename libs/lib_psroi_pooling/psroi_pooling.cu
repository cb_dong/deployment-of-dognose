#include <cfloat>
#include "megbrain_pubapi.h"
#include <iostream>


#define CUDA_KERNEL_LOOP(i, n) \
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; \
       i < (n); \
       i += blockDim.x * gridDim.x)

#define CAFFE_CUDA_NUM_THREADS 512
#define CUDA_CHECK(condition) \
  /* Code block avoids redefinition of cudaError_t error */ \
  do { \
    cudaError_t error = condition; \
    if (error != cudaSuccess) { \
      std::cout << " " << cudaGetErrorString(error); \
    } \
  } while (0)

inline static int CAFFE_GET_BLOCKS(const int N) {
  return (N + CAFFE_CUDA_NUM_THREADS - 1) / CAFFE_CUDA_NUM_THREADS;
}

// Perform the PSROIPooling Forward
__global__ void PSROIPoolingForward(const int nThreads, const float* rois_data, const float spatial_scale, const float * fm_data, const int fm_channels, const int fm_height, const int fm_width, float* output_data, const int pooled_channel, const int pooled_height, const int pooled_width, const int group_size){
    CUDA_KERNEL_LOOP(index, nThreads){
        // compute the 2-d coordinates of the target feature map
        int pw = index % pooled_width;
        int ph = (index / pooled_width) % pooled_height;
        int ctop = (index / pooled_width / pooled_height) % pooled_channel;
        // n for rois_idx, maybe no use for our implementation
        int n = index / pooled_width / pooled_height / pooled_channel;

        rois_data += n * 5;
        // get the batch id
        int roi_batch_ind = rois_data[0];
        float roi_start_w = static_cast<float>(round(rois_data[1])) * spatial_scale;
        float roi_start_h = static_cast<float>(round(rois_data[2])) * spatial_scale;
        float roi_end_w = static_cast<float>(round(rois_data[3]) + 1.) * spatial_scale;
        float roi_end_h = static_cast<float>(round(rois_data[4]) + 1.) * spatial_scale;

        float roi_width = max(roi_end_w - roi_start_w,  0.1);
        float roi_height = max(roi_end_h - roi_start_h, 0.1);

        float bin_size_h = roi_height / static_cast<float>(pooled_height);
        float bin_size_w = roi_width  / static_cast<float>(pooled_width);
        
        int hstart = floor(static_cast<float>(ph) * bin_size_h + roi_start_h);
        int wstart = floor(static_cast<float>(pw) * bin_size_w + roi_start_w);
        int hend   = ceil(static_cast<float>(ph + 1) * bin_size_h + roi_start_h);
        int wend   = ceil(static_cast<float>(pw + 1) * bin_size_w + roi_start_w);

        hstart = min(max(hstart, 0), fm_height);
        hend = min(max(hend, 0), fm_height);
        wstart = min(max(wstart, 0), fm_width);
        wend = min(max(wend, 0), fm_width);

        bool is_empty = (hend <= hstart) || (wend <= wstart);

        // int c  = (ctop * pooled_height + ph) * pooled_width + pw;
        int gw = floor(static_cast<float>(pw)* group_size / pooled_width);
        int gh = floor(static_cast<float>(ph)* group_size / pooled_height);
        gw = min(max(gw, 0), group_size - 1);
        gh = min(max(gh, 0), group_size - 1);
        int c = (ctop*group_size + gh)*group_size + gw;

        fm_data += (roi_batch_ind * fm_channels + c) * fm_height * fm_width;
        float out_sum = 0;
        for(int h = hstart; h < hend; ++h){
            for(int w = wstart; w < wend; ++w){
                int fm_index = h * fm_width + w;
                out_sum += fm_data[fm_index];
            }
        }
        float bin_area = (hend - hstart) * (wend - wstart);
        output_data[index] = is_empty? 0. : out_sum / bin_area;
    }
}
// Perform the PSROIPooling Backward
__global__ void PSROIPoolingBackward(const int nThreads, const float* rois_data, const float spatial_scale, const float* output_diff, const int pooled_channel, const int pooled_height, const int pooled_width, const int group_size, float* fm_diff, const int fm_channels, const int fm_height, const int fm_width){
    CUDA_KERNEL_LOOP(index, nThreads){
        int pw = index % pooled_width;
        int ph = (index / pooled_width) % pooled_height;
        int ctop = (index / pooled_width / pooled_height) % pooled_channel;
        int n = index / pooled_width / pooled_height / pooled_channel;

        rois_data += n * 5;
        int roi_batch_ind = rois_data[0];
        float roi_start_w = static_cast<float>(round(rois_data[1])) * spatial_scale;
        float roi_start_h = static_cast<float>(round(rois_data[2])) * spatial_scale;
        float roi_end_w = static_cast<float>(round(rois_data[3]) + 1.) * spatial_scale;
        float roi_end_h = static_cast<float>(round(rois_data[4]) + 1.) * spatial_scale;

        float roi_width = max(roi_end_w - roi_start_w,  0.1);
        float roi_height = max(roi_end_h - roi_start_h, 0.1);

        float bin_size_h = roi_height / static_cast<float>(pooled_height);
        float bin_size_w = roi_width  / static_cast<float>(pooled_width);
        
        int hstart = floor(static_cast<float>(ph) * bin_size_h + roi_start_h);
        int wstart = floor(static_cast<float>(pw) * bin_size_w + roi_start_w);
        int hend   = ceil(static_cast<float>(ph + 1) * bin_size_h + roi_start_h);
        int wend   = ceil(static_cast<float>(pw + 1) * bin_size_w + roi_start_w);

        hstart = min(max(hstart, 0), fm_height);
        hend = min(max(hend, 0), fm_height);
        wstart = min(max(wstart, 0), fm_width);
        wend = min(max(wend, 0), fm_width);
        
        bool is_empty = (hend <= hstart) || (wend <= wstart);
        // int c  = (ctop * pooled_height + ph) * pooled_width + pw;
        int gw = floor(static_cast<float>(pw)* group_size / pooled_width);
        int gh = floor(static_cast<float>(ph)* group_size / pooled_height);
        gw = min(max(gw, 0), group_size - 1);
        gh = min(max(gh, 0), group_size - 1);
        int c = (ctop*group_size + gh)*group_size + gw;
        
        // float* offset_fm_diff = fm_diff + (roi_batch_ind * fm_channels +c) * fm_height * fm_width;
        fm_diff += (roi_batch_ind * fm_channels +c) * fm_height * fm_width;
        float bin_area = (hend - hstart) * (wend - wstart);

        float diff_val = is_empty ? 0. : output_diff[index] / bin_area;
        for (int h = hstart; h < hend; ++h){
            for (int w = wstart; w < wend; ++w){
                int bottom_index = h*fm_width + w;
                atomicAdd(fm_diff+bottom_index, diff_val);
            }
        }
    }
}

extern "C"{
    
    using MGBDevTensor = mgb::pubapi::DeviceTensor; 
    using std::cout;

    void PSROIPoolingForwardGpu(void* rois_ptr, void* fm_ptr, void* output_ptr, const float spatial_scale, const int pooled_channel, const int pooled_height, const int pooled_width, const int group_size){
        auto rois_tensor = mgb::pubapi::as_versioned_obj<MGBDevTensor>(rois_ptr);
        auto fm_tensor = mgb::pubapi::as_versioned_obj<MGBDevTensor>(fm_ptr);
        auto output_tensor = mgb::pubapi::as_versioned_obj<MGBDevTensor>(output_ptr);

        // auto cuda_stream = static_cast<cudaStream_t> (rois_tensor->desc.cuda_ctx.stream);
        auto cuda_stream = static_cast<cudaStream_t> (output_tensor->desc.cuda_ctx.stream);

        int nr_rois = rois_tensor->desc.shape[0];
        int output_count = nr_rois * pooled_channel * pooled_height * pooled_width;

        const float * rois_data = static_cast<const float * > (rois_tensor->desc.dev_ptr);
        const float * fm_data   = static_cast<const float * > (fm_tensor->desc.dev_ptr);
        float * output_data     = static_cast<float* > (output_tensor->desc.dev_ptr);
        auto fm_shape = fm_tensor->desc.shape;

        PSROIPoolingForward<<< CAFFE_GET_BLOCKS(output_count), CAFFE_CUDA_NUM_THREADS, 0, cuda_stream >>> (output_count, rois_data, spatial_scale, fm_data, fm_shape[1], fm_shape[2], fm_shape[3], output_data, pooled_channel, pooled_height, pooled_width, group_size);
    }

    void PSROIPoolingBackwardGpu(void * rois_ptr, void * output_diff_ptr, void * fm_diff_ptr, float spatial_scale, const int pooled_channel, const int pooled_height, const int pooled_width, const int group_size){
        
        auto rois_tensor = mgb::pubapi::as_versioned_obj<MGBDevTensor>(rois_ptr);
        auto fm_diff_tensor = mgb::pubapi::as_versioned_obj<MGBDevTensor>(fm_diff_ptr);  
        auto output_diff_tensor = mgb::pubapi::as_versioned_obj<MGBDevTensor>(output_diff_ptr);

        // auto cuda_stream = static_cast<cudaStream_t> (rois_tensor->desc.cuda_ctx.stream);
        auto cuda_stream = static_cast<cudaStream_t> (output_diff_tensor->desc.cuda_ctx.stream);
        auto fm_diff_shape = fm_diff_tensor->desc.shape;

        int fm_diff_count = fm_diff_shape[0] * fm_diff_shape[1] * fm_diff_shape[2] * fm_diff_shape[3];

        float * fm_diff = static_cast<float * > (fm_diff_tensor->desc.dev_ptr);
        CUDA_CHECK(cudaMemsetAsync(fm_diff, 0, sizeof(float) * fm_diff_count, cuda_stream));

        int nr_rois = rois_tensor->desc.shape[0];
        int output_count = nr_rois * pooled_channel * pooled_height * pooled_width;

        const float * rois_data = static_cast<const float * >(rois_tensor->desc.dev_ptr);
        const float * output_diff = static_cast<const float * >(output_diff_tensor->desc.dev_ptr);
        PSROIPoolingBackward<<< CAFFE_GET_BLOCKS(output_count), CAFFE_CUDA_NUM_THREADS, 0, cuda_stream >>> (output_count, rois_data, spatial_scale, output_diff, pooled_channel, pooled_height, pooled_width, group_size, fm_diff, fm_diff_shape[1], fm_diff_shape[2], fm_diff_shape[3]);
    }

}
