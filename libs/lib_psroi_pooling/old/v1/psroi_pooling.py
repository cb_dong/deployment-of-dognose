#!/usr/bin/env mdl
# This file will seal the psroi_pooling opr
import numpy as np
import ctypes
import megbrain as mgb
from megbrain.craniotome import CraniotomeBase
from megbrain.craniotome import make_opr
from megskull.opr.all import MGBOprForwarderBase
from megskull.graph import as_varnode
import os, sys, struct
from IPython import embed
from time import time

_TYPE_POINTER = ctypes.c_void_p
_TYPE_INT = ctypes.c_int32
_TYPE_FLOAT = ctypes.c_float

_so_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lib_psroi_pooling.so')
_so_lib  = ctypes.CDLL(_so_path)

_so_lib.PSROIPoolingForwardGpu.argtypes = [_TYPE_POINTER, _TYPE_INT, _TYPE_FLOAT, _TYPE_POINTER, _TYPE_INT, _TYPE_INT, _TYPE_INT, _TYPE_POINTER, _TYPE_INT, _TYPE_INT, _TYPE_INT]
_so_lib.PSROIPoolingForwardGpu.restype = None

_so_lib.PSROIPoolingBackwardGpu.argtypes = [_TYPE_POINTER, _TYPE_INT, _TYPE_FLOAT, _TYPE_POINTER, _TYPE_INT, _TYPE_INT, _TYPE_INT, _TYPE_POINTER, _TYPE_INT, _TYPE_INT, _TYPE_INT, _TYPE_INT]
_so_lib.PSROIPoolingBackwardGpu.restype = None


_so_path_2 = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lib_psroi_pooling2.so')
_so_lib2  = ctypes.CDLL(_so_path_2)

_so_lib2.PSROIPoolingForwardGpu.argtypes = [_TYPE_POINTER, _TYPE_POINTER, _TYPE_POINTER, _TYPE_FLOAT, _TYPE_INT, _TYPE_INT, _TYPE_INT]
_so_lib2.PSROIPoolingForwardGpu.restype = None

_so_lib2.PSROIPoolingBackwardGpu.argtypes = [_TYPE_POINTER, _TYPE_POINTER, _TYPE_POINTER, _TYPE_FLOAT, _TYPE_INT, _TYPE_INT, _TYPE_INT]
_so_lib2.PSROIPoolingBackwardGpu.restype = None


class PSROIPoolingBackwardImpl(CraniotomeBase):
    __nr_inputs__ = 3
    __nr_outputs__= 1
    def setup(self, spatial_scale, pooled_height, pooled_width):
        self._spatial_scale = spatial_scale
        self._pooled_height = pooled_height
        self._pooled_width = pooled_width
        self._phw = self._pooled_width * self._pooled_height

    def execute(self, inputs, outputs):
        rois, feat_map, pspooled_grad = inputs
        feat_map_grad = outputs[0]

        # rois_dev_ptr = rois.dev_ptr
        # pspooled_grad_dev_ptr = pspooled_grad.dev_ptr
        # feat_map_grad_dev_ptr = feat_map_grad.dev_ptr

        rois_dev_ptr = rois.pubapi_dev_tensor_ptr
        pspooled_grad_dev_ptr = pspooled_grad.pubapi_dev_tensor_ptr
        feat_map_grad_dev_ptr = feat_map_grad.pubapi_dev_tensor_ptr

        init_value = feat_map_grad.get_value()

        start_time = time()
        _so_lib.PSROIPoolingBackwardGpu(rois_dev_ptr, rois.shape[0], self._spatial_scale, pspooled_grad_dev_ptr, self._pooled_channel, self._pooled_height, self._pooled_width, feat_map_grad_dev_ptr, feat_map_grad.shape[0], feat_map_grad.shape[1], feat_map_grad.shape[2], feat_map_grad.shape[3])
        end_time = time()
        elapse_time_1 = end_time - start_time

        old_value = feat_map_grad.get_value()

        feat_map_grad.set_value(np.zeros(old_value.shape))
        zero_value = feat_map_grad.get_value()

        rois_tensor_ptr = rois.pubapi_dev_tensor_ptr
        pspooled_grad_tensor_ptr = pspooled_grad.pubapi_dev_tensor_ptr
        feat_map_grad_tensor_ptr = outputs[0].pubapi_dev_tensor_ptr

        start_time = time()
        _so_lib2.PSROIPoolingBackwardGpu(rois_tensor_ptr, pspooled_grad_tensor_ptr, feat_map_grad_tensor_ptr, self._spatial_scale, self._pooled_channel, self._pooled_height, self._pooled_width)
        end_time = time()
        elapse_time_2 = end_time - start_time

        new_value = outputs[0].get_value()
        print('PSPooled Backward Time Metric, old: %.4f, new: %.4f, nr_rois: %d, rms_value: %.2f' % (elapse_time_1, elapse_time_2, rois.shape[0], np.sum(np.abs(new_value-old_value))))
        # embed()


    def infer_shape(self, inp_shapes):
        # infer the output shapes of the PSROIPooling
        rois_shape = inp_shapes[0]
        fm_shape = inp_shapes[1]

        self._pooled_channel = fm_shape[1] / (self._phw)
        assert self._pooled_channel == int(self._pooled_channel)
        self._pooled_channel = int(self._pooled_channel)
        return (fm_shape, )

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        return 0

class PSROIPoolingImpl(CraniotomeBase):
    __nr_inputs__ = 2
    __nr_outputs__= 2

    def setup(self, spatial_scale, pooled_height, pooled_width):
        self._spatial_scale = spatial_scale
        self._pooled_height = pooled_height
        self._pooled_width = pooled_width
        self._phw = self._pooled_width * self._pooled_height

    def execute(self, inputs, outputs):
        # invoke the
        rois, feat_map = inputs
        pspooled_map = outputs[0]
        rois_tensor_ptr = rois.pubapi_dev_tensor_ptr
        fm_tensor_ptr = feat_map.pubapi_dev_tensor_ptr
        pspooled_tensor_ptr = outputs[1].pubapi_dev_tensor_ptr
        start_time = time()
        _so_lib2.PSROIPoolingForwardGpu(rois_tensor_ptr, fm_tensor_ptr, pspooled_tensor_ptr, self._spatial_scale, self._pooled_channel, self._pooled_height, self._pooled_width)
        end_time = time()
        elapse_time_2 = end_time - start_time

        # rois_dev_ptr = rois.dev_ptr
        # fm_dev_ptr = feat_map.dev_ptr
        # pspooled_dev_ptr = pspooled_map.dev_ptr

        # prev_value = pspooled_map.get_value()
        # _so_lib.PSROIPoolingForwardGpu(rois_dev_ptr, rois.shape[0], self._spatial_scale, fm_dev_ptr, feat_map.shape[1], feat_map.shape[2], feat_map.shape[3], pspooled_dev_ptr, self._pooled_channel, self._pooled_height, self._pooled_width)
        rois_tensor_ptr = rois.pubapi_dev_tensor_ptr
        fm_tensor_ptr = feat_map.pubapi_dev_tensor_ptr
        pspooled_tensor_ptr = outputs[0].pubapi_dev_tensor_ptr
        start_time = time()
        _so_lib.PSROIPoolingForwardGpu(rois_tensor_ptr, rois.shape[0], self._spatial_scale, fm_tensor_ptr, feat_map.shape[1], feat_map.shape[2], feat_map.shape[3], pspooled_tensor_ptr, self._pooled_channel, self._pooled_height, self._pooled_width)
        end_time = time()
        elapse_time_1 = end_time - start_time
        old_value = pspooled_map.get_value()
        new_value = outputs[1].get_value()


        print('PSPooled Forward Time Metric, old: %.4f, new: %.4f, nr_rois: %d, rms_value: %.2f' % (elapse_time_1, elapse_time_2, rois.shape[0], np.sum(np.abs(new_value-old_value))))
        # post_value = pspooled_map.get_value()

    def get_serialize_params(self):
        return ('psroi_pooling', struct.pack('fii', self._spatial_scale, self._pooled_height, self._pooled_width))

    def grad(self, wrt_idx, inputs, outputs, out_grad):
        if wrt_idx == 1:
            return PSROIPoolingBackwardImpl.make(inputs[0], inputs[1], out_grad[0], spatial_scale=self._spatial_scale, pooled_height=self._pooled_height, pooled_width=self._pooled_width)
        else:
            return 0

    def infer_shape(self, inp_shapes):
        # infer the output shapes of the PSROIPooling
        rois_shape = inp_shapes[0]
        fm_shape = inp_shapes[1]

        self._pooled_channel = fm_shape[1] / (self._phw)
        assert self._pooled_channel == int(self._pooled_channel)
        self._pooled_channel = int(self._pooled_channel)
        return [[rois_shape[0], self._pooled_channel, self._pooled_height, self._pooled_width], [rois_shape[0], self._pooled_channel, self._pooled_height, self._pooled_width]]

# decorates the RPNTargetCran as the Megskull Operator
class PSROIPooling(MGBOprForwarderBase):
    def __init__(self, name, rois, feat_map, spatial_scale, pooled_height, pooled_width):
        rois = as_varnode(rois)
        feat_map = as_varnode(feat_map)
        super().__init__(name=name, inputs=[rois, feat_map])
        self._spatial_scale = spatial_scale
        self._pooled_height= pooled_height
        self._pooled_width = pooled_width

    def _mgb_func(self, env, *inputs):
        rois, feat_map = inputs
        pooled_fm, _ = PSROIPoolingImpl.make(rois, feat_map, spatial_scale=self._spatial_scale, pooled_height=self._pooled_height, pooled_width=self._pooled_width)
        return pooled_fm
