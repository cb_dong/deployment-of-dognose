import torchvision
import torch.nn as nn
import torchvision
import torch.nn as nn
import re
from functools import partial
from timm.models.efficientnet import tf_efficientnet_b7_ns
import torch
from torch.nn.modules.dropout import Dropout
from torch.nn.modules.linear import Linear
from torch.nn.modules.pooling import AdaptiveAvgPool2d

class ResNet(nn.Module):
    def __init__(self, name, out_dim, pre_trained):
        super(ResNet, self).__init__()
        if '50' in name:
            res = torchvision.models.resnet50(pretrained=pre_trained)
        elif '34' in name:
            res = torchvision.models.resnet34(pretrained=pre_trained)
        else:
            raise Exception('invalid ResNet model {}'.format(name))
            
        self.fconv  =  nn.Sequential(*list(res.children())[:4])
        self.layer1 = res.layer1
        self.layer2 = res.layer2
        self.layer3 = res.layer3
        self.layer4 = res.layer4
        self.avgpool= res.avgpool
        self.fc = nn.Sequential(
            nn.Linear(2048,out_dim,bias=True),
            nn.Dropout(0.5),
            )
        
    def forward(self,x):
        x = self.fconv(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        out = self.fc(x)
        return out, x


encoder_params = {
    "tf_efficientnet_b7_ns": {
        "features": 2560,
        "init_op": partial(tf_efficientnet_b7_ns, pretrained=True, drop_path_rate=0.2)
    },
}

class Efficientnet(nn.Module):
    def __init__(self, encoder, out_dim=2,dropout_rate=0.0) -> None:
        super().__init__()
        self.encoder = encoder_params[encoder]["init_op"]()
        self.avg_pool = AdaptiveAvgPool2d((1, 1))
        self.dropout = Dropout(dropout_rate)
        self.fc = Linear(encoder_params[encoder]["features"], out_dim)

    def forward(self, x):
        x = self.encoder.forward_features(x)
        x = self.avg_pool(x).flatten(1)
        x = self.dropout(x)
        out = self.fc(x)
        return out,x