import os

class Config:
    det_short_size = 600
    det_max_size = 1000
    image_size = (600, 600)
    # det_short_size = 600
    # det_max_size = 1000
    # image_size = (600, 600)

    ld_size = (112, 112)
    reco_size = (224, 224)
    qua_size = (128, 128)

    test_cls_threshold = 0.5
    test_nms = 0.5
    test_nms_version = 'softnms'
    dist_func = 'euclidean'
    badcase = False
    # badcase_path = '/unsullied/sharefs/wangbin03/isilon-home/FastAI/data/debug/DogNose/badcase/'
    # if badcase:
    #     if not os.path.exists(badcase_path):
    #         os.makedirs(badcase_path)

    img_path = './test.jpg'
    detection_model = 'models/detection/4-1.shuffleNetV2.600.model'
    landmark_model = 'models/landmark/best-84'
    recogntion_model = 'models/recogntion/recogntion.tar'
    distance_th = 25
    outdim = 316
    blur_th = 100

config = Config()
