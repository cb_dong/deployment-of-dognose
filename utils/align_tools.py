import os
import argparse
import time
import cv2
import numpy as np
import pickle as pkl

########## shape index ###############
#ld_name = ['contour_chin', 'contour_left1', 'contour_left2', 'contour_left3', 'contour_left4', 'contour_left5', 'contour_left6', 'contour_left7', 'contour_left8', 'contour_left9', 'contour_right1', 'contour_right2', 'contour_right3', 'contour_right4', 'contour_right5', 'contour_right6', 'contour_right7', 'contour_right8', 'contour_right9', 'left_eye_bottom', 'left_eye_left_corner', 'left_eye_lower_left_quarter', 'left_eye_lower_right_quarter', 'left_eye_pupil', 'left_eye_right_corner', 'left_eye_top', 'left_eye_upper_left_quarter', 'left_eye_upper_right_quarter', 'left_eyebrow_left_corner', 'left_eyebrow_lower_left_quarter', 'left_eyebrow_lower_middle', 'left_eyebrow_lower_right_quarter', 'left_eyebrow_right_corner', 'left_eyebrow_upper_left_quarter', 'left_eyebrow_upper_middle', 'left_eyebrow_upper_right_quarter', 'mouth_left_corner', 'mouth_lower_lip_bottom', 'mouth_lower_lip_left_contour1', 'mouth_lower_lip_left_contour2', 'mouth_lower_lip_left_contour3', 'mouth_lower_lip_right_contour1', 'mouth_lower_lip_right_contour2', 'mouth_lower_lip_right_contour3', 'mouth_lower_lip_top', 'mouth_right_corner', 'mouth_upper_lip_bottom', 'mouth_upper_lip_left_contour1', 'mouth_upper_lip_left_contour2', 'mouth_upper_lip_left_contour3', 'mouth_upper_lip_right_contour1', 'mouth_upper_lip_right_contour2', 'mouth_upper_lip_right_contour3', 'mouth_upper_lip_top', 'nose_contour_left1', 'nose_contour_left2', 'nose_contour_left3', 'nose_contour_lower_middle', 'nose_contour_right1', 'nose_contour_right2', 'nose_contour_right3', 'nose_left', 'nose_right', 'nose_tip', 'right_eye_bottom', 'right_eye_left_corner', 'right_eye_lower_left_quarter', 'right_eye_lower_right_quarter', 'right_eye_pupil', 'right_eye_right_corner', 'right_eye_top', 'right_eye_upper_left_quarter', 'right_eye_upper_right_quarter', 'right_eyebrow_left_corner', 'right_eyebrow_lower_left_quarter', 'right_eyebrow_lower_middle', 'right_eyebrow_lower_right_quarter', 'right_eyebrow_right_corner', 'right_eyebrow_upper_left_quarter', 'right_eyebrow_upper_middle', 'right_eyebrow_upper_right_quarter']
#ld_name_flip = ["contour_chin", "contour_right1", "contour_right2", "contour_right3", "contour_right4", "contour_right5", "contour_right6", "contour_right7", "contour_right8", "contour_right9", "contour_left1", "contour_left2", "contour_left3", "contour_left4", "contour_left5", "contour_left6", "contour_left7", "contour_left8", "contour_left9", "right_eye_bottom", "right_eye_right_corner", "right_eye_lower_right_quarter", "right_eye_lower_left_quarter", "right_eye_pupil", "right_eye_left_corner", "right_eye_top", "right_eye_upper_right_quarter", "right_eye_upper_left_quarter", "right_eyebrow_right_corner", "right_eyebrow_lower_right_quarter", "right_eyebrow_lower_middle", "right_eyebrow_lower_left_quarter", "right_eyebrow_left_corner", "right_eyebrow_upper_right_quarter", "right_eyebrow_upper_middle", "right_eyebrow_upper_left_quarter", "mouth_right_corner", "mouth_lower_lip_bottom", "mouth_lower_lip_right_contour1", "mouth_lower_lip_right_contour2", "mouth_lower_lip_right_contour3", "mouth_lower_lip_left_contour1", "mouth_lower_lip_left_contour2", "mouth_lower_lip_left_contour3", "mouth_lower_lip_top", "mouth_left_corner", "mouth_upper_lip_bottom", "mouth_upper_lip_right_contour1", "mouth_upper_lip_right_contour2", "mouth_upper_lip_right_contour3", "mouth_upper_lip_left_contour1", "mouth_upper_lip_left_contour2", "mouth_upper_lip_left_contour3", "mouth_upper_lip_top", "nose_contour_right1", "nose_contour_right2", "nose_contour_right3", "nose_contour_lower_middle", "nose_contour_left1", "nose_contour_left2", "nose_contour_left3", "nose_right", "nose_left", "nose_tip", "left_eye_bottom", "left_eye_right_corner", "left_eye_lower_right_quarter", "left_eye_lower_left_quarter", "left_eye_pupil", "left_eye_left_corner", "left_eye_top", "left_eye_upper_right_quarter", "left_eye_upper_left_quarter", "left_eyebrow_right_corner", "left_eyebrow_lower_right_quarter", "left_eyebrow_lower_middle", "left_eyebrow_lower_left_quarter", "left_eyebrow_left_corner", "left_eyebrow_upper_right_quarter", "left_eyebrow_upper_middle", "left_eyebrow_upper_left_quarter"]
ld_name=range(28)
ld_name_flip=[0,1,3,2,11,10,9,8,7,6,5,4,20,21,22,23,24,25,26,27,12,13,14,15,16,17,18,19]
ld_name_flip_idx = [ld_name.index(i) for i in ld_name_flip]
ld_name_flip_idx = np.array(ld_name_flip_idx)

########## mean shape 2*81 (final_dim) ###############
#mean_face = np.fromfile(mean_face_path).reshape(-1, 2)
#mean_shape=np.array( [[0.003335515123848594,0.45466550556673585],[-0.3853725740784002,-0.16585241162027106],[-0.38468755216074413,-0.07695772853951822],[-0.37735986336141486,0.010273098674278038],[-0.3618490940799354,0.09760654593474598],[-0.33563310637549676,0.1823677015068859],[-0.29432969385810087,0.26028670032204954],[-0.23856330281232171,0.32851737243615625],[-0.17177596212204463,0.3873806669711914],[-0.09381092213017785,0.4353723337737229],[0.3828081266681486,-0.17067415615043371],[0.3838581795910509,-0.08257272951361767],[0.3783174600951948,0.004486499872676886],[0.36471649053696004,0.09158653509898666],[0.3407262866982372,0.17654415512880753],[0.3012190204117782,0.25466481655497314],[0.24665961051933186,0.3236686880408765],[0.18041562034394834,0.38405495766042547],[0.10274004509031635,0.43344309000708264],[-0.17969973483295257,-0.1440087282567218],[-0.25260547198334765,-0.16569448189377892],[-0.21994087489113115,-0.15101598163110078],[-0.13870704914692553,-0.1475366713350584],[-0.17607383017492048,-0.17284541531431194],[-0.10195022418237716,-0.15420609512833702],[-0.17716727279102681,-0.1947084369261461],[-0.21865649801125928,-0.18762424921742482],[-0.13479740618059763,-0.18325597960933118],[-0.3104054792602325,-0.24766999896982436],[-0.25800994659534804,-0.2545286200449921],[-0.20102898773123648,-0.25687477630844313],[-0.14242495890601917,-0.251773886096892],[-0.08300263952765115,-0.25180718489061593],[-0.2640328171573194,-0.29077054574236944],[-0.20205073148345462,-0.2999267068023861],[-0.13759601653224443,-0.2893816523742017],[-0.146175258569511,0.20184062800020322],[-0.0007910960839028909,0.2832566869161382],[-0.07506223444614214,0.2253297560434299],[-0.10924911023638924,0.2419308253197984],[-0.06442596219579279,0.27165391091197016],[0.07450233583120085,0.22367883509322675],[0.10907108834379346,0.24085584242733207],[0.06182652194311726,0.2712991836726581],[-0.0006870343779886307,0.23579422729029034],[0.14496230697011084,0.19943081596239345],[-0.0007661288200644529,0.2064676258436892],[-0.0362251111179276,0.17030565723292726],[-0.09476001238280386,0.1823746132427029],[-0.07314300319478102,0.20145840028751208],[0.03424546545672273,0.16962484326764796],[0.09232870755646487,0.1812509562315651],[0.07227536309659392,0.20062567423746916],[-0.0008860808719115138,0.17726036392702718],[-0.05644410462116607,-0.152499901943668],[-0.08692638623298828,-0.008176721121233042],[-0.06067849231117746,0.08436916955813638],[-0.0008724636217114096,0.10207064024689409],[0.05498746838578843,-0.1522459095183217],[0.08274861414641436,-0.00899693146273075],[0.058652585613234984,0.0838511930302848],[-0.11319509683945518,0.054872904918927136],[0.10870080811835252,0.05557350605424619],[-0.0018245362456886943,0.03571644397029659],[0.18105843335440733,-0.14440633447283033],[0.09898540775364355,-0.15427084321195447],[0.13789871763141695,-0.14751103387168088],[0.21885069017169637,-0.15240149931275807],[0.1736007169074926,-0.17356136273397701],[0.24912095012147664,-0.16667389986653156],[0.17630408862336502,-0.19509713901470696],[0.1333995269639419,-0.18427460299360562],[0.2168825542197074,-0.1876236708612942],[0.0813819936303997,-0.2543566730350753],[0.14377828245179572,-0.25324356269972487],[0.20395203730589143,-0.25818110324703],[0.25789741864497867,-0.2561175665113649],[0.3070606789201931,-0.24971009615263046],[0.13851148722459486,-0.29249422621362287],[0.20280605662415532,-0.3022945912973523],[0.26305746144631675,-0.29198726532849173]], dtype='float64')
mean_shape=np.array([[-0.01475394,0.34025299],
 [ 0.00996227, -0.230026  ],
 [-0.37604893, -0.0329068 ],
 [ 0.37395107,  0.01770857],
 [ 0.18273905, -0.21823525],
 [ 0.30731235, -0.14280618],
 [ 0.29278443,  0.19014503],
 [ 0.166796,    0.28746204],
 [-0.18356464,  0.27999583],
 [-0.31288046,  0.16858714],
 [-0.29572599, -0.17163693],
 [-0.16175656, -0.2322682 ],
 [-0.21016662,  0.06489354],
 [-0.19542122, -0.11588285],
 [-0.26386774, -0.04552826],
 [-0.13186938, -0.00202224],
 [-0.14981707, -0.0748398 ],
 [-0.16160261,  0.04097062],
 [-0.26002989,  0.02652535],
 [-0.23685082, -0.09325985],
 [ 0.21067209,  0.08041392],
 [ 0.20198049, -0.10069779],
 [ 0.26791535, -0.03239724],
 [ 0.12953422,  0.01854185],
 [ 0.15123366, -0.05336665],
 [ 0.15682858,  0.06011513],
 [ 0.26108446,  0.04899854],
 [ 0.24156184, -0.07873652]])

# landmark 8pts
#0 contour_chin
#1 contour_left1
#10 contour_right1
#23 left_eye_pupil
#68 right_eye_pupil
#63 nose_tip
#36 mouth_left_corner
#45 mouth_right_corner

########## align function ###############
def rot_scale_align(src, dst):
    """
    np.dot(src, mat) = dst
    """
    src_x, src_y = src[:, 0], src[:, 1]
    dst_x, dst_y = dst[:, 0], dst[:, 1]
    d = (src**2).sum()
    a = sum(src_x*dst_x + src_y*dst_y) / d
    b = sum(src_x*dst_y - src_y*dst_x) / d
    mat = np.array([[a, -b], [b, a]])
    return mat

def alignto(lm, mean_face, output_size):
    lm = np.array(lm).reshape(-1, 2)
    mean = lm.mean(axis=0)
    mat1 = rot_scale_align(lm - mean, mean_face).T
    # print (mat1)
    mat1 *= output_size
    # print (mat1)
    # convert np.dot(lm-mean, mat1) to np.dot(lm, mat2.T), thus mat2 should absorb -mean
    mat2 = np.float64([[mat1[0][0], mat1[1][0], -mean[0] * mat1[0][0] - mean[1] * mat1[1][0] + output_size[0] // 2],
                       [mat1[0][1], mat1[1][1], -mean[0] * mat1[0][1] - mean[1] * mat1[1][1] + output_size[1] // 2]])
    return mat2

def align_to_mean_face(ld, inpsize):
    ld = np.array(ld).reshape(-1, 2)
    return alignto(ld[:28,:], mean_shape, inpsize)

def lm_affine(lm, mat):
    lm = np.array(lm).reshape(-1, 2)
    return np.dot(np.concatenate([lm, np.ones((lm.shape[0], 1))], axis=1), mat.T)

def inv_mat(mat):
    return np.linalg.inv(mat.tolist() + [[0, 0, 1]])[:2]

def split(label):
    comps = dict()
    comps['contour'] = label[:, :19*2]
    comps['eye'] = np.concatenate([label[:, 19*2:28*2], label[:, 64*2:73*2]], axis=1)
    comps['eyebrow'] = np.concatenate([label[:, 28*2:36*2], label[:, 73*2:]], axis=1)
    comps['mouth'] = label[:, 36*2:54*2]
    comps['nose'] = label[:, 54*2:64*2]
    return comps
            
def showimg(img, ld):
    ld = ld.reshape(-1,2)
    for i in range(len(ld)):
        cv2.circle(img, (int(ld[i,0]), int(ld[i,1])), 3, (255,0,0), -1)
    cv2.imshow('ttt', img)
    cv2.waitKey(0)

def format_time(elapse):
    elapse = int(elapse)
    hour = elapse // 3600
    minute = elapse % 3600 // 60
    seconds = elapse % 60
    return "{:02d}:{:02d}:{:02d}".format(hour, minute, seconds)



