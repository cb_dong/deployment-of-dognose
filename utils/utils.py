# -*- coding: utf-8 -*-
import cv2
import numpy as np
from scipy.spatial.distance import cdist



from utils.align_tools import *
from albumentations import DualTransform, ImageOnlyTransform
from albumentations.augmentations.functional import crop
from albumentations import Compose, RandomBrightnessContrast, \
    HorizontalFlip, FancyPCA, HueSaturationValue, OneOf, ToGray, \
    ShiftScaleRotate, ImageCompression, PadIfNeeded, GaussNoise, GaussianBlur
from albumentations.pytorch.functional import img_to_tensor
import torch

def isotropically_resize_image(img, size, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC):
    h, w = img.shape[:2]
    if max(w, h) == size:
        return img
    if w > h:
        scale = size / w
        h = h * scale
        w = size
    else:
        scale = size / h
        w = w * scale
        h = size
    interpolation = interpolation_up if scale > 1 else interpolation_down
    resized = cv2.resize(img, (int(w), int(h)), interpolation=interpolation)
    return resized


class IsotropicResize(DualTransform):
    def __init__(self, max_side, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC,
                 always_apply=False, p=1):
        super(IsotropicResize, self).__init__(always_apply, p)
        self.max_side = max_side
        self.interpolation_down = interpolation_down
        self.interpolation_up = interpolation_up

    def apply(self, img, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC, **params):
        return isotropically_resize_image(img, size=self.max_side, interpolation_down=interpolation_down,
                                          interpolation_up=interpolation_up)

    def apply_to_mask(self, img, **params):
        return self.apply(img, interpolation_down=cv2.INTER_NEAREST, interpolation_up=cv2.INTER_NEAREST, **params)

    def get_transform_init_args_names(self):
        return ("max_side", "interpolation_down", "interpolation_up")


def img_to_tenosr(img):
    img = img.transpose(2, 0, 1)
    img = img[None, :, :, :]
    img = np.ascontiguousarray(img)
    return img

def get_hw_by_short_size(im_height, im_width, short_size, max_size):
    im_size_min = np.min([im_height, im_width])
    im_size_max = np.max([im_height, im_width])
    scale = (short_size + 0.0) / im_size_min
    if scale * im_size_max > max_size:
        scale = (max_size + 0.0) / im_size_max

    resized_height, resized_width = int(round(im_height * scale)), int(
        round(im_width * scale))
    return resized_height, resized_width


def resize_img_by_short_and_max_size(
        img, short_size, max_size, *, random_scale_methods=False):
    resized_height, resized_width = get_hw_by_short_size(
        img.shape[0], img.shape[1], short_size, max_size)
    scale = resized_height / (img.shape[0] + 0.0)

    chosen_resize_option = cv2.INTER_LINEAR
    if random_scale_methods:
        resize_options = [cv2.INTER_CUBIC, cv2.INTER_LINEAR, cv2.INTER_NEAREST,
                          cv2.INTER_AREA, cv2.INTER_LANCZOS4]
    img = cv2.resize(img, (resized_width, resized_height),
                     interpolation=chosen_resize_option)
    return img, scale

def bbox_transform_inv(boxes, deltas):
    if boxes.shape[0] == 0:
        return np.zeros((0, deltas.shape[1]), dtype=deltas.dtype)

    boxes = boxes.astype(deltas.dtype, copy=False)
    widths = boxes[:, 2] - boxes[:, 0] + 1.0
    heights = boxes[:, 3] - boxes[:, 1] + 1.0
    ctr_x = boxes[:, 0] + 0.5 * widths
    ctr_y = boxes[:, 1] + 0.5 * heights

    dx = deltas[:, 0::4]
    dy = deltas[:, 1::4]
    dw = deltas[:, 2::4]
    dh = deltas[:, 3::4]

    pred_ctr_x = dx * widths[:, np.newaxis] + ctr_x[:, np.newaxis]
    pred_ctr_y = dy * heights[:, np.newaxis] + ctr_y[:, np.newaxis]
    pred_w = np.exp(dw) * widths[:, np.newaxis]
    pred_h = np.exp(dh) * heights[:, np.newaxis]

    pred_boxes = np.zeros(deltas.shape, dtype=deltas.dtype)
    # x1
    pred_boxes[:, 0::4] = pred_ctr_x - 0.5 * pred_w
    # y1
    pred_boxes[:, 1::4] = pred_ctr_y - 0.5 * pred_h
    # x2
    pred_boxes[:, 2::4] = pred_ctr_x + 0.5 * pred_w
    # y2
    pred_boxes[:, 3::4] = pred_ctr_y + 0.5 * pred_h

    return pred_boxes

def _clip_boxes(boxes, im_shape):
    """Clip boxes to image boundaries."""
    boxes[:, 0::4] = np.maximum(boxes[:, 0::4], 0)
    boxes[:, 1::4] = np.maximum(boxes[:, 1::4], 0)
    boxes[:, 2::4] = np.minimum(boxes[:, 2::4], im_shape[1] - 1)
    boxes[:, 3::4] = np.minimum(boxes[:, 3::4], im_shape[0] - 1)
    return boxes

def align_image(ldpts, img, scale=(600, 600)):
    mat = align_to_mean_face(ldpts, scale)
    aligned_ldmks = []
    for ld in ldpts:
        aligned_ldmks.append(np.dot(mat, np.array([ld[0],ld[1],1.0])))
    img = cv2.warpAffine(img, mat, (scale[0], scale[1]), borderMode=cv2.BORDER_REPLICATE)
    return img, np.array(aligned_ldmks)

def nose_mask(img, ld, pad_h, pad_w):
    h, w = img.shape[0], img.shape[1]
    min_x = int(max(np.min(ld[:, 0]) - pad_w, 0))
    max_x = int(min(np.max(ld[:, 0]) + pad_w, w))
    min_y = int(max(np.min(ld[:, 1]) - pad_h, 0))
    max_y = int(min(np.max(ld[:, 1]) + pad_h, h))
    mask = img[min_y:max_y, min_x:max_x, :]
    return mask

def get_img(nori_id):
    img = imdecode(nr.get(nori_id))
    return img

def search(querys, gallery, features_query, features_gallery, query_labels, gallery_labels, dist_func=None):

    dists = cdist(features_query, features_gallery, dist_func)

    aps = np.zeros((len(querys),))
    cmc = np.zeros((len(querys), len(gallery)), dtype='float')

    idxs = []
    for i, x in enumerate(querys):
        if x['person_id'] not in gallery_labels:
            continue
        idxs.append(i)
        order = dists[i, :].argsort()
        # if config.badcase:
        #     if x['person_id'] != gallery[order[0]]['person_id']:
        #         gall_img = get_img(gallery[order[0]])
        #         query_img = get_img(x)
        #         for o in range(len(order)):
        #             if x['person_id'] == gallery[order[o]]['person_id']:
        #                 positive_img = get_img(gallery[order[o]])
        #                 img = np.concatenate([gall_img, query_img, positive_img], axis=1)
        #                 cv2.imwrite(config.badcase_path + '{}.jpg'.format(i), img)
        #                 break
        aps[i], cmc[i, :] = compute_ap(x, gallery, order)

    aps = aps[idxs]
    cmc = cmc[idxs, :]
    return aps, cmc

def compute_ap(query, gallery, order):
    good = intersect_size = ap = old_recall = counter = 0
    old_precision = 1.0
    cmc = np.zeros(len(gallery))
    ngood = len([i for i in range(len(gallery)) if (gallery[order[i]]['person_id'] == query['person_id'])]) #  and gallery[order[i]]['quality'] != 'junk')])
    for i in range(len(gallery)):
        idx = order[i]
        flag = False
        if gallery[idx]['person_id'] == query['person_id']:
            good += 1
            cmc[i:] = 1
            flag = True
        counter = counter + 1
        if flag:
            intersect_size = intersect_size + 1
        recall = intersect_size / ngood
        precision = intersect_size / counter
        ap = ap + (recall - old_recall) * (old_precision + precision) / 2.0
        old_recall = recall
        old_precision = precision
        if good == ngood:
            break
    return ap, cmc

def get_angle_rate(ld):
    l_l = ld[2]
    l_m = ld[15]
    r_m = ld[23]
    r_r = ld[3]
    l_dis = abs(l_m[0] - l_l[0])
    r_dis = abs(r_m[0] - r_r[0])
    rate = l_dis / r_dis
    return rate

def get_blur_score(image):
    img2gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_resize = cv2.resize(img2gray, (224, 224))
    score = cv2.Laplacian(img_resize, cv2.CV_64F).var()
    return score

def crop_patch(img, ld):
    left = int(ld[15][0])
    right = int(ld[23][0])
    top = int(ld[1][1])
    down = int(min(ld[15][1], ld[23][1]))
    patch = img[top:down, left:right]
    return patch

def crop_patchs(img, ld):
    left1 = int(ld[13][0])
    right1 = int(ld[1][0])
    top1 = int(ld[1][1])
    down1 = int(ld[13][1])
    patch1 = img[top1:down1, left1:right1]

    left2 = int(ld[1][0])
    right2 = int(ld[21][0])
    top2 = int(ld[1][1])
    down2 = int(ld[21][1])
    patch2 = img[top2:down2, left2:right2]

    left3 = int(ld[16][0])
    right3 = int(ld[25][0])
    top3 = int(ld[16][1])
    down3 = int(ld[25][1])
    patch3 = img[top3:down3, left3:right3]
    return patch1, patch2, patch3

def get_edge_sobel(img):
    img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    sobelX = cv2.Sobel(img,cv2.CV_64F,1,0)
    sobelY = cv2.Sobel(img,cv2.CV_64F,0,1)

    sobelX = np.uint8(np.absolute(sobelX))
    sobelY = np.uint8(np.absolute(sobelY))

    sobelCombined = cv2.bitwise_or(sobelX,sobelY)
    return sobelCombined

def sigmoid(x):
    return 1 / (1 + np.exp(-5*x))


        
transforms = Compose([IsotropicResize(max_side=448, interpolation_down=cv2.INTER_AREA, interpolation_up=cv2.INTER_CUBIC),
        PadIfNeeded(min_height=448, min_width=448, border_mode=cv2.BORDER_CONSTANT),
    ])
     
