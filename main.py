import io
import flask
from PIL import Image
from models import Efficientnet
import cv2
import torch
import numpy as np
from scipy.spatial.distance import cdist

from megskull.graph import Function
from meghair.utils import io
from configs.config_wb import config as cfg
from modules import detection_nose, landmark_nose
from utils import utils
from albumentations.pytorch.functional import img_to_tensor
import pickle


app = flask.Flask(__name__)
model = None
device = torch.device('cpu')
fails = []
max_len = 5

def load_megbrain(model_file):
    network = io.load_network(model_file)
    func = Function().compile(network.outputs)
    return func

def load_model():
    """Load the pre-trained model, you can use your model just as easily.
    """
    global detection_func,landmark_func,recognition_func
    detection_func = load_megbrain(cfg.detection_model)
    landmark_func = load_megbrain(cfg.landmark_model)

    recognition_func = Efficientnet(encoder="tf_efficientnet_b7_ns", out_dim=316)
    state_dict = torch.load(cfg.recogntion_model, map_location='cpu')
    recognition_func.load_state_dict({k: w for k, w in state_dict.items() if ('fc.0' not in k)},strict=False)
    recognition_func = recognition_func.eval().to(device)

def save_database():
    with open('database.pkl','wb') as f:
        pickle.dump(database,f)

def load_database():
    global base_feats,labels,database
    try:
        with open('database.pkl','rb') as f:
            database = pickle.load(f)
        for key in database.keys():
            if len(database[key]) > max_len:
                database[key] = database[key][:max_len]
            elif len(database[key]) < max_len:
                database[key].extend(database[key][:max_len-len(database[key])])

        save_database()
        base_feats = np.array(database.values())
        labels = list(database.keys())
    except:

        base_feats = np.random.random((200,max_len,2560))
        labels = np.random.random(200).tolist()
        database = dict(zip(labels, base_feats))


def get_query_feat(img):
    if_blur = 0
    if_nose = 0
    query_feat = None
    with torch.no_grad():
        blur = utils.get_blur_score(img)
        if blur < cfg.blur_th:
            if_blur = 1
            return if_blur,if_nose,query_feat

        im = detection_nose(img.astype(np.float32), detection_func, cfg)
        if im is None:
            if_nose = 1
            return if_blur, if_nose, query_feat

        im = im.astype(np.uint8)
        align_im = landmark_nose(im, landmark_func, cfg)
        align_im = cv2.cvtColor(align_im, cv2.COLOR_BGR2RGB)
        image = utils.transforms(image=align_im)
        inp = image["image"]
        inp = img_to_tensor(inp, {"mean": [0.485, 0.456, 0.406], "std": [0.229, 0.224, 0.225]})
        inp = inp.unsqueeze(0).to(device)
        _, feat = recognition_func(inp)

        query_feat = feat[0].cpu().numpy().reshape(1, -1)
        return if_blur, if_nose, query_feat


@app.route("/query", methods=["POST"])
def query():
    data = {"success": False}
    if flask.request.method == 'POST':
        if flask.request.files.get("image"):
            image = flask.request.files["image"].read()
            image = Image.open(io.BytesIO(image))
            img = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)

            blur, nose, query_feat = get_query_feat(img)
            import pdb
            pdb.set_trace()
            if blur:
                data['success'] = False
                data['error_reason'] = "too blur"
                return flask.jsonify(data)

            if nose:
                data['success'] = False
                data['error_reason'] = "found no nose"
                return flask.jsonify(data)

            dists = np.mean([cdist(query_feat, base_feats[:, i], "euclidean") for i in range(max_len)], axis=0).reshape(-1)
            ix = np.argmin(dists)
            if dists[ix] < cfg.distance_th:
                label = labels[ix]
                data['query_result'] = label

                data['feature'] = query_feat.reshape(-1).tolist()
                data['success'] = True
                return flask.jsonify(data)
            else:
                data['success'] = False
                data['error_reason'] = "found no such dog"
                data['feature'] = query_feat.reshape(-1).tolist()
                return flask.jsonify(data)


@app.route("/insert", methods=["POST"])
def insert():
    data = {"success": False}
    if flask.request.method == 'POST':
        if flask.request.files.get("image") and flask.request.files.get("label"):
            image = flask.request.files["image"].read()
            label = int(flask.request.files["label"].read())
            image = Image.open(io.BytesIO(image))
            img = cv2.cvtColor(np.asarray(image), cv2.COLOR_RGB2BGR)

            blur, nose, query_feat = get_query_feat(img)
            import pdb
            pdb.set_trace()
            if blur:
                data['success'] = False
                data['error_reason'] = "too blur"
                return flask.jsonify(data)

            if nose:
                data['success'] = False
                data['error_reason'] = "found no nose"
                return flask.jsonify(data)
            if label in labels:
                if len(database[label]) == max_len:
                    temp = np.array(database[label])
                    temp[:max_len-1] = database[label][1:]
                    temp[-1] = query_feat.reshape(-1)
                    database[label] = temp.tolist()
                else:
                    database[label].append(query_feat.reshape(-1).tolist())
            else:
                database[label] = [query_feat.reshape(-1).tolist()]

            save_database()
            load_database()

            data['success'] = True
            return flask.jsonify(data)

if __name__ == '__main__':
    print("Loading PyTorch model and Flask starting server ...")
    print("Please wait until server has fully started")
    load_model()
    load_database()
    app.debug = True
    app.run()