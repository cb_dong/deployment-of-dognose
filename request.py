import requests
import argparse

# Initialize the PyTorch REST API endpoint URL.
PyTorch_REST_API_URL = 'http://127.0.0.1:5000/'


def insert(image_path,label):
    image = open(image_path, 'rb').read()
    payload = {'image': image, 'label': label}
    r = requests.post(PyTorch_REST_API_URL+'insert', files=payload).json()
    if r['success']:
        print(r)
    else:
        print('Request failed')
        print(r)

def query_result(image_path):
    # Initialize image path
    image = open(image_path, 'rb').read()
    payload = {'image': image}

    # Submit the request.
    r = requests.post(PyTorch_REST_API_URL+'query', files=payload).json()

    # Ensure the request was successful.
    if r['success']:
        print(r)
        # Loop over the predictions and display them.
        # for (i, result) in enumerate(r['predictions']):
        #     print('{}. {}: {:.4f}'.format(i + 1, result['label'],
        #                                   result['probability']))
    # Otherwise, the request failed.
    else:
        print('Request failed')
        print(r)


if __name__ == '__main__':
    file = 'dog_nose7.png'
    #query_result(file)
    insert(file,1)
